//1) Enum 4 new features in ES 6, describe them and provide a code sample for each one.
//A. Template literals
let name = "Gilbert";
let message = `hello ${name}`
console.log(message); 

//B. Destructuring
let object = {id: 80, name: "gilbert", mail:"gilbertguitar15"};
let {id, mail} = object;
console.log(id); // 80
console.log(mail); // 20

//C. Constants
const a = 'Hola AccelOne';
a = 'Bye'; //throws an error

//D. Spread operator
const person = { name: 'Gilberto', age: 30 };
const clonedPersonObj = {...person}; // { name: 'Gilberto', age: 30 }


//2) Which is the difference between ‘==’ and ‘===’

// === is a strict comparation (value and type)
// ==  is a weaker comparation (just value) 


//3) Which of the following is correct:

//All are correct


//4) Provide a sample of a Person class declaration with a name property, initialized to John.

class Person {
    constructor(name) {
      this.name = "John"; 
    }
}


//5) Provide the code to create a Human class with a property age, and inherit the Person class you created in the previous point from Human

class Human extends Person {
    constructor (name, age) {
        super(name)
        this.age = age
    }
}


//6) Given this code:
const numbers = [1, 2, 3, 4];
const extendedNumbers = [...oldArray, 5];
//Which are the values in extendedNumbers?

// "oldArray" is not defined, but if you do:
const extendedNumbers = [...numbers, 5];
//it will return [1, 2, 3, 4, 5]

//7) Given the following code:
const oldObject = {
name: 'Max'
 };
 const newObject = {
...oldObject,
age: 28
 };
//What does newObject look like?

//it wolud look like this: {name: "Max", age: 28}

//8) Define a Closure and provide a code snippet as a sample.

//a closure is when a function is able to remember and access a lexical scope

function Greet() {
    let name = 'Gilbert';
    
    function showName() {
      console.log(name);
    }  

    return showName;
}

let sayGilbert = greet();
  
sayGilbert();

 
//9) What is a Promise for? Provide a code snippet as a sample

//a promise is a feature in ES6 to handle asynchonous calls (to an API for example) and it allows the code to continue without blocking the execution

function getContent() {
    return new Promise(function(res, reject) {
        //some code
    })
}

getContent().then(res =>{
    console.log(res);
  }).catch(() => {
    console.log("Error: something didn't work");
  }); 


  
//10) Create a multidimensional array to store the names and grades of 5 students. Then write a function that calculates the average grade of the class.
  
let grades = [
    ["Jose", 7],
    ["Pedro", 8],
    ["Pablo", 4],
    ["Carlos", 6],
    ["marcos", 3]
];


const average = (grades) => {
    let sum = 0
    let contador = 0

    for (let i of grades) {
        for (let j of i) {
            if(typeof j === "number"){
                sum = sum + j
                contador++            
            }
        }
    }
        
    return sum / contador;
}
  
average(grades)

//11) Create a function that receives a positive integer (and 0) and returns the Fibonacci list for the provided number. The function should only accept integer values and 0, and through and error otherwise.

function fib(num){
    if(typeof num == "number"){
       if(num <= 2) return 1;        
       return fib(num -1) + fib(num -2);        
    }else{
        return "error"
    }
}

console.log(fib(4));  // 3
console.log(fib(10)); // 55
console.log(fib(28)); // 317811
console.log(fib(35)); // 9227465

//12) Which is the library used to make ES 6 compatible with all the up to date browsers? Please describe what it basically does.

//Babel: it's a transpiler that converts javascript (ES6, 7, 8) into old javascript (ES5) and make it work in all browsers

//It also make all the syntactic sugar to work in ES5

